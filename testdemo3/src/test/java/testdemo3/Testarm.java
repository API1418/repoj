package testdemo3;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Testarm {
		static Armstrong obj;
		@BeforeAll
		public static void ba() {
			obj = new Armstrong();
			System.out.println("I am BeforeAll");
		}
		
		@BeforeEach
		public void be() {
			System.out.println("I am BeforeEach");
		}
		
		@Test
		public void t1() {
			assertEquals(obj.arm(2),true);
			System.out.println("I am Testcase 1");
		}
		
		@Test
		public void t2() {
			assertEquals(obj.arm(153),true);
			System.out.println("I am Testcase 2");
		}
		
		@AfterAll
		public static void aa() {
			System.out.println("I am AfterAll");
		}
		
		@AfterEach
		public void ae() {
			System.out.println("I am AfterEach");
		}

}

